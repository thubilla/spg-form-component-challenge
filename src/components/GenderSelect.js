import React from 'react'

const GenderSelect = props => {
  return (
    <label htmlFor={props.el_id}>{props.label || 'Sex'} 
      <select id={props.el_id} name={props.form_name}>
        <option value="">Choose One</option>
        {props.options.map((op, i) => {
          return <option key={i} value={op.value}>{op.text_content}</option>
        })} 
      </select>
    </label>
  )
}

export default GenderSelect
