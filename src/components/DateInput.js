import React from 'react'

const DateInput = props => {
  return (
    <label htmlFor={props.el_id}>{props.label || 'Date of Birth'}
      <input 
        id={props.el_id} 
        name={props.form_name || 'dob' } 
        type="date" 
        max={props.maxdate}
        required={props.required} />
    </label>
  )
}

export default DateInput
