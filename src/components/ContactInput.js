import React from 'react'

const ContactInput = props => {
  return (
  <section>
    <label htmlFor={props.el_id}>{props.label || 'Contact Number'}
      <input 
        id={props.el_id} 
        type="text" 
        name={props.form_name} 
        required={props.required} />
    </label>
    <select id={props.el_id + '_type'} name={props.form_name + '_type'}>
      <option value="">Choose Type</option>
      {props.type_options.map((op, i) => {
        return <option value={op.value} key={i}>{op.text_content}</option>
      })}
    </select>
  </section>
  )
}

export default ContactInput
