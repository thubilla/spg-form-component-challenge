import React, { Component } from 'react'
import schema from '../assets/schema.json'

import FormComponent from './FormComponent'

class App extends Component {
  render() {
    return (
      <div className="App">
        <FormComponent {...schema}  />
      </div>
    );
  }
}

export default App
