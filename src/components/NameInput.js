import React from 'react'

const NameInput = (props) => {
  return (
    <label htmlFor={props.el_id}> {props.label || 'Name'}
      <input 
        id={props.el_id} 
        name={props.form_name || 'name'} 
        type="text" 
        pattern="\w+\s\w+"
        required={props.required} />
    </label>
  )
}

export default NameInput
