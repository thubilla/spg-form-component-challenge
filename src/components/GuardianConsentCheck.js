import React from 'react'
import NameInput from './NameInput'
import ContactInput from './ContactInput'

class GuardianConsentCheck extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      requiresConsent: props.default_value 
    }

    this.toggleConsent = this.toggleConsent.bind(this)
  }

  toggleConsent () {
    this.setState({
      requiresConsent: !this.state.requiresConsent
    })
  }

  render () {
    return (
      <fieldset>
        <legend>{this.props.label}</legend>
        <label htmlFor={this.props.el_id}>Required
          <input 
            id={this.props.el_id}
            name={this.props.form_name}
            type="checkbox" 
            checked={this.state.requiresConsent} 
            onChange={this.toggleConsent}/>
        </label>

        {
          !this.state.requiresConsent 
          ? ''
          : <NameInput form_name="guardian_name" required="true" label="Guardian Name" />
        }

        {
          !this.state.requiresConsent 
          ? ''
          : <ContactInput form_name="guardian_contact" required="true" type_options={this.props.contactTypeOptions} label="Guardian Contact" />
        }
      </fieldset>
    ) 
  }
}

export default GuardianConsentCheck
