import React from 'react'
import NameInput from './NameInput'
import DateInput from './DateInput'
import GenderSelect from './GenderSelect'
import ContactInput from './ContactInput'
import GuardianConsentCheck from './GuardianConsentCheck'

class FormComponent extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      formData: {} 
    }

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (e) {
    e.preventDefault()

    var entries = Array.from(new FormData(e.target).entries())
    var generalData = entries.reduce(this._reduceFormData, {})
    var contactDetailsArray = Array.from(e.target.querySelectorAll('input[name="contact"]')).reduce(this._reduceContactData, []) 
    var formData = { 
      name: generalData.name,
      date_of_birth: generalData.dob,
      gender: generalData.gender,
      contact: contactDetailsArray,
    }

    if(generalData.guardian) {
      formData.guardian = {
        name: generalData.guardian_name,
        contact: generalData.guardian_contact,
        contact_type: generalData.guardian_contact_type
      } 
    }

    this.setState({ formData })
  }

  _reduceFormData (accumulator, [key, value]) {
    accumulator[key] = value
    return accumulator 
  }

  _reduceContactData (accumulator, node) {
    accumulator.push({
      value: node.value,
      type: node.parentNode.nextElementSibling.value
    }) 

    return accumulator
  }

  render () {
    var { name, date_of_birth: dob, gender, guardian, contact } = this.props

    return (
      <form onSubmit={this.handleSubmit}>
        <NameInput {...name} />
        <DateInput {...dob} />
        <GenderSelect {...gender} />
        <ContactInput {...contact} label="Primary Contact Number" />
        <ContactInput {...contact} label="Secondary Contact Number" />
        <ContactInput {...contact} label="Other Contact Number" />
        <GuardianConsentCheck {...guardian} contactTypeOptions={contact.type_options} />
        <pre>{JSON.stringify(this.state.formData, null, 2)}</pre>
        <button type="submit">Submit</button>
      </form>
    ) 
  }
}

export default FormComponent
